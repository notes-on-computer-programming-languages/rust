# rust

A language to build reliable and efficient software. https://www.rust-lang.org

[[_TOC_]]

![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=rustc%20ocaml-base-nox%20ghc%20julia%20scala%20racket%20elixir%20clojure%20chezscheme&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)

# Official documentation
* [*Learn Rust*](https://www.rust-lang.org/learn)
* [*The Rust Programming Language*](https://doc.rust-lang.org/stable/book/)
  2018 Book by Steve Klabnik and Carol Nichols, with contributions from the Rust Community
* [Blog](https://blog.rust-lang.org/)

# General Books
Other books are listed in specialized paragraphs below this text.
* [*Comprehensive Rust 🦀*
  ](https://google.github.io/comprehensive-rust/)
  * https://github.com/google/comprehensive-rust
* *Rust GUI Development for Linux*
  2024-07 Alfredo Deza, Noah Gift (video at Pragmatic AI Labs)
* *Idiomatic Rust*
  2024-09 Brenden Matthews (Audiobook at Manning Publications)
  * Fluent interfaces for creating delightful APIs
  * The Builder pattern to encapsulate data and perform initialization
  * Immutable data structures that help you avoid hard-to-debug data race conditions
  * Functional programming patterns
  * Anti-patterns and what not to do in Rust
* *Rust Fundamentals*
  2024-06 Alfredo Deza (course at Pragmatic AI Labs)
* *Write Powerful Rust Macros*
  2024-06 Sam Van Overmeire (Manning Publications)
* *Effective Rust*
  2024-04 David Drysdale (O'Reilly Media, Inc.)
* *Learn Rust in a Month of Lunches*
  2024-04 David MacLeod (Manning Publications)
* *Game Development with Rust and WebAssembly*
  2022-04 Eric Smith (Packt Publishing)
* *Command-Line Rust*
  2022-01 Ken Youens-Clark (O'Reilly Media, Inc.)
* *Practical webassembly : explore the fundamentals of webassembly programming using rust*
  **2022**
* *Rust for Rustaceans*
  2021-12 Jon Gjengset (No Starch Press)

# Unofficial documentation
* [Rust (Computer program language)
  ](https://www.worldcat.org/search?q=su%3ARust+%28Computer+program+language%29)
* [*Learn X in Y minutes, Where X=Rust*
  ](https://learnxinyminutes.com/docs/rust)
* [rust language tutorial](https://google.com/search?q=rust+language+tutorial)
* [Rust Tutorials, Courses, and Books](https://gitconnected.com/learn/rust)
---
* [*Why Dark didn't choose Rust*
  ](https://blog.darklang.com/why-dark-didnt-choose-rust/)
  2020-11 Paul Biggar
* [*Rust and SPARK: Software Reliability for Everyone*
  ](https://www.electronicdesign.com/markets/automation/article/21804924/rust-and-spark-software-reliability-for-everyone)
  2020-04 Quentin Ochem
* [*Why we chose Rust as our programming language*
  ](https://bitbucket.org/blog/why-rust)
  2019-06 Mohit Agrawal
* [*When Rust is safer than Haskell*
  ](https://www.fpcomplete.com/blog/when-rust-is-safer-than-haskell/)
  2019-01 Michael Snoyman
* [*Introducing the Rust crash course*
  ](https://www.snoyman.com/blog/2018/10/introducing-rust-crash-course/)
  2018-10 Michael Snoyman
  * RSS: https://www.snoyman.com/series/rust-crash-course/rss.xml
* [*A Gentle Introduction To Rust*](https://stevedonovan.github.io/rust-gentle-intro/readme.html)
  2017-2018 Steve Donovan
* [*Learning Rust*](https://medium.com/learning-rust/rust-basics-e73304ab35c7)
  2016-01 Dumindu Madunuwan
* [*Rust Programming Language Tutorial*](https://www.javatpoint.com/rust-tutorial)
* [*Rust Tutorial*](https://www.tutorialspoint.com/rust/)

# Language features
## Async-await
* [*Async-await hits beta!*
  ](https://blog.rust-lang.org/2019/09/30/Async-await-hits-beta.html)
  2019-09 Niko Matsakis

## Closure
* [*Why is Fn derived from FnMut (which is derived from FnOnce)?*
  ](https://stackoverflow.com/questions/31190851/why-is-fn-derived-from-fnmut-which-is-derived-from-fnonce)
  (2016)
* [*Finding Closure in Rust*
  ](http://huonw.github.io/blog/2015/05/finding-closure-in-rust/)
  2015-05 Huon Wilson

## Functional programming
* [*Rust for Haskell Programmers!*
  ](https://mmhaskell.com/rust)
  (2022) Monday Morning Haskell
* [*Get Ready for Rust!*
  ](https://mmhaskell.com/blog/2019/11/18/get-ready-for-rust)
  2019-11 Monday Morning Haskell
  * "Rust is a very interesting language to compare to Haskell."
* [*Easy functional programming techniques in Rust for everyone*
  ](https://deepu.tech/functional-programming-in-rust/)
  2019-11 Deepu K Sasidharan
* [*Learn Rust With Entirely Too Many Linked Lists*
  ](https://rust-unofficial.github.io/too-many-lists/index.html)
  (2018)
* [rust functional programming](https://google.com/search?q=rust+functional+programming)
  * [*Functional Programming in Rust - Part 1 : Function Abstraction*
  ](http://blog.madhukaraphatak.com/functional-programming-in-rust-part-1/)
  2016 Madhukar
  * [*Is Rust functional?*](https://www.fpcomplete.com/blog/2018/10/is-rust-functional)
  2018 Michael Snoyman
  * [*Rust for functional programmers*](https://science.raphael.poss.name/rust-for-functional-programmers.html)
  2014 Raphael ‘kena’ Poss

### Books
* [*Hands-On functional programming in Rust :*
  ](https://www.worldcat.org/search?q=Hands-On+functional+programming+in+Rust)
  *build modular and reactive applications with functional programming techniques in Rust*
  2018 Andrew Johnson
  * [Packt](https://www.packtpub.com/product/hands-on-functional-programming-in-rust/9781788839358)
  * [GitHub](https://github.com/packtpublishing/hands-on-functional-programming-in-rust)

### Higher Kinded Types
* [*Idiomatic monads in Rust, A pragmatic new design for high-level abstractions*
  ](https://varkor.github.io/blog/2019/03/28/idiomatic-monads-in-rust.html)
  2019-03 varkor
* [*Rust/Haskell: Higher-Kinded Types (HKT)*
  ](https://gist.github.com/CMCDragonkai/a5638f50c87d49f815b8)
  2015-06 CMCDragonkai
  * Cites: [14427/hkt.rs
    ](https://gist.github.com/14427/af90a21b917d2892eace)
    * Which has been forked and updated for newer compiler's versions by [badboy
      ](https://gist.github.com/badboy/6954cc6ce1c71b921094)
  * Found a little after a search for [programming language for higher kinded types
    ](https://google.com/search?q=programming+language+for+higher+kinded+types)


## Interfaces (see Traits)

## Iterators and Streams
* [*Iterators and Streams in Rust and Haskell*
  ](https://www.fpcomplete.com/blog/2017/07/iterators-streams-rust-haskell/)
  2017-07 Michael Snoyman

## Memory management
* [*An Overview of Memory Management in Rust*](https://pcwalton.github.io/2013/03/18/an-overview-of-memory-management-in-rust.html)
  2013-03 pcwalton

### Resource acquisition is initialization (RAII)
#### Official documentation
* [*RAII*](https://doc.rust-lang.org/rust-by-example/scope/raii.html)

#### Unofficial documentation
* [rust resource acquisition is initialization (RAII)
  ](https://google.com/search?q=rust+Resource+acquisition+is+initialization+%28RAII%29)
* [*Resource acquisition is initialization*](https://en.m.wikipedia.org/wiki/Resource_acquisition_is_initialization)
* [*Rust Means Never Having to Close a Socket*
  ](https://blog.skylight.io/rust-means-never-having-to-close-a-socket/)
  2014-10 Yehuda Katz

### Rust reference counting
* [rust reference counting](https://google.com/search?q=rust+reference+counting)
* [*Rc\<T\>, the Reference Counted Smart Pointer*
  ](https://doc.rust-lang.org/1.22.0/book/second-edition/ch15-04-rc.html)

## MUSL support for fully static binaries
* [*MUSL support for fully static binaries*
  ](https://doc.rust-lang.org/edition-guide/rust-2018/platform-and-target-support/musl-support-for-fully-static-binaries.html)
  The Edition Guide
* MUSL is available on many distributions ([repology](https://repology.org/project/musl/versions))!
* Rust may need to be downloaded in MUSL version or compiled with MUSL on plateforms using mostly
  another library, in order to be able to produce fully static binaries.

## Traits
* [*Abstraction without overhead: traits in Rust*
  ](https://blog.rust-lang.org/2015/05/11/traits.html)
  2015-05 Aaron Turon (Rust Blog)

# Libraries
## Full-text search
* [tantivy-search/tantivy](https://github.com/tantivy-search/tantivy)
  Tantivy is a full-text search engine library inspired by Apache Lucene and
  written in Rust

## Databases
* https://crates.io/categories/database
* sea-query: A dynamic query builder for MySQL, Postgres and SQLite 
* rusqlite: Ergonomic wrapper for SQLite
* sqlx: async, pure Rust SQL (explained in Rust Web Development with Rocket 2022 Karuna Murti)
* postgres:
  * [*Establishing a Postgres Connection*
    ](https://mmhaskell.com/rust-web/postgres)
    (2022) Monday Morning Haskell
* diesel:
  * [*Diesel: Making a Formal Schema*
    ](https://mmhaskell.com/rust-web/diesel)
    (2022) Monday Morning Haskell

## HTTP
### hyper (server and client)
* [hyper](https://crates.io/crates/hyper)
* [rust:hyper](https://repology.org/project/rust:hyper/versions)
* https://hyper.rs/guides

### Web server framework
* [web server framework](https://crates.io/search?q=web%20server%20framework&sort=recent-downloads)
* wrap has Server-Sent Events and Websockets

### Rocket
* [rocket.rs](https://rocket.rs)
* [*Rust Web Skills*
](https://mmhaskell.com/rust-web)
  (2022) Monday Morning Haskell
  * [*Rocket: Making a Web Server in Rust*
    ](https://mmhaskell.com/rust-web/rocket)
    (2022) Monday Morning Haskell
* Based on hyper via rocket_http
* Crate rocket_db_pools

### Books
* *Rust Web Programming - Third Edition*
  2024-10 Maxwell Flitton (Packt Publishing)
  * 2023-01
  * 2021
* *Rust Web Development*
  2023-02 Bastian Gruber (Manning)
  * 2022-12
  * Video Edition
* Rust Web Development with Rocket
  2022-06 Karuna Murti (Packt)
  * Database with SQLx
* Practical Rust Web Projects Building Cloud and Web-Based Applications
  2021 Shing Lyu (Apress)

## Languages implemented in Rust
* [lisp](https://crates.io/search?q=lisp)
* [scheme](https://crates.io/search?q=scheme)

## Parallel and concurrent programming
### Software transactional memory
* Crate stm https://docs.rs/stm/0.1.2/stm/
  * https://docs.rs/stm/0.1.2/stm/#stm-safety

## Parser combinator
* [monadic parser combinator library parsec rust
  ](https://www.google.com/search?q=monadic+parser+combinator+library+parsec+rust)
* [*Learning Parser Combinators With Rust*](https://bodil.lol/parser-combinators/)
  2019-04
* [Parser tooling](https://lib.rs/parsing) @lib.rs
* [All Crates for keyword 'parser-combinators'
  ](https://crates.io/keywords/parser-combinators)
* Search Results [monadic parser combinator
  ](https://crates.io/search?q=monadic%20parser%20combinator)
  * [chomp](https://crates.io/crates/chomp)
### ...
* [d-plaindoux/celma](https://github.com/d-plaindoux/celma) @github

## REPL
* [State of rust repls?](https://users.rust-lang.org/t/state-of-rust-repls/22942)
* [evcxr_repl](https://crates.io/crates/evcxr_repl)
* keyword/[repl](https://crates.io/keywords/repl)

## Scripting
* [*Using Rust for ‘Scripting’*
  ](https://v4.chriskrycho.com/2016/using-rust-for-scripting.html)
  2016-11 Chris Krycho

## Static site generators
* [*Top 3 Rust static site generators and when to use them*
  ](https://blog.logrocket.com/top-3-rust-static-site-generators-and-when-to-use-them/)
  2020-10 Michiel Mulders
* [*Building a static site generator in 100 lines of Rust*
  ](https://kerkour.com/rust-static-site-generator/)
  2021-09 Sylvain Kerkour

## Wayland
* [software-packages-demo/wayland](https://gitlab.com/software-packages-demo/wayland)

# Tools
* [33 Rust Static Analysis Tools
  ](https://www.analysis-tools.dev/tag/rust)
